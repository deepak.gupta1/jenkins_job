#!/bin/bash

awk 'BEGIN{
FS=","
print "<HTML><BODY>""<TABLE border="3"><TH>Instance_Name</TH><TH>Instance_ID</TH><TH>CPU_Utilization</TH><TH>Network_Input</TH><TH>Network_Output</TH>" 
}
{
printf "<TR>"
for(i=1;i<=NF;i++)
{
printf "<TD>%s</TD>", $i
}
print "</TR>"
 }
END{
print "</TABLE></BODY></HTML>"
 }
' result.csv > result.html
