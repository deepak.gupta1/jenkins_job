#!/bin/bash


operation=$1

instanceReport() {
region=$1

echo -e "\nListing Instances in region:'$region'..." 
aws ec2 describe-instances --region ${region} --filters Name=instance-state-name,Values=running > instance.json
jq -r '.Reservations[] | ['.Instances[].InstanceId','.Instances[].Tags[0].Value'] | @csv' instance.json | tr -d '"' > list.csv
report=`jq -r '.Reservations[] | ['.Instances[].InstanceId','.Instances[].Tags[0].Value'] | @csv' instance.json | tr -d '"'` 
echo "These are the listed Instances 
${report}" 

}


if [[ $operation == 'instanceReport' ]]
then 
	region=$2
	instanceReport $2
fi
