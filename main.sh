#!/bin/bash


CPUUtilization_LOW_THRESHOLD=$1
CPUUtilization_HIGH_THRESHOLD=$2
NetworkIn_LOW_THRESHOLD=$3
NetworkIn_HIGH_THRESHOLD=$4
NetworkOut_LOW_THRESHOLD=$5
NetworkOut_HIGH_THRESHOLD=$6

getMetrics () {
	instance_id=$1
	metric_name=$2
	startTime=`date +"%Y-%m-%d"T"%T" --date='10  minutes ago'`
	endTime=`date +"%Y-%m-%d"T"%T"`

	aws cloudwatch get-metric-statistics --namespace AWS/EC2 --metric-name ${metric_name}  --period 60 --statistics Average --dimensions Name=InstanceId,Value=${instance_id} --start-time ${startTime} --end-time ${endTime} | jq '.Datapoints[0].Average'    

}

getMetricsUtilizationReport() {
        instance_id=$1
        metric_name=$2

        value=`getMetrics $instance_id $metric_name`
        metricsValue=$(expr $value*100 | bc)
        LOW_THRESHOLD=${metric_name}_LOW_THRESHOLD
        HIGH_THRESHOLD=${metric_name}_HIGH_THRESHOLD


        if [[ ${metricsValue%%.*} -lt ${LOW_THRESHOLD%%.*} ]]
        then
                echo "Low"
        elif [[ ${metricsValue%%.*} -gt ${HIGH_THRESHOLD%%.*} ]]
        then
                echo "High"
        else
                echo "Moderate"
        fi
}
       

function getColumn() {
    COLUMN=$1
    STRING=$2
    echo "${STRING}"  | cut -d"," -f${COLUMN}
}

echo "" > result.csv
while IFS= read -r line
do
    instance_id=`getColumn 1 $line`
    instance_name=`getColumn 2 $line`
    cpu=`getMetricsUtilizationReport ${instance_id} CPUUtilization`
    networkIn=`getMetricsUtilizationReport ${instance_id} NetworkIn`
    networkOut=`getMetricsUtilizationReport ${instance_id} NetworkOut`

    echo "${instance_name}, ${instance_id}, ${cpu}, ${networkIn}, ${networkOut}" >> result.csv
done < list.csv
